import { NextPage } from 'next';

const Home: NextPage = () => {
  return (
    <section className="flex justify-center items-center min-h-screen">
      <h1 className="text-xl">Web Kesiswaan Stemba 🏫</h1>
    </section>
  );
};

export default Home;
