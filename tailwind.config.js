/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./pages/**/*.{ts,tsx}', './components/**/*.{ts,tsx}'],
  theme: {
    extend: {},
  },
  daisyui: {
    themes: ['corporate'],
  },
  plugins: [require('daisyui')],
};
